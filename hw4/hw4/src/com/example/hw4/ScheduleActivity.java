package com.example.hw4;
import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import org.htmlcleaner.*;


public class ScheduleActivity extends Activity{
    private List<HashMap<String, Object>> mData;
    private final String XPATH_TRAIN = "//span[@id='classname']", XPATH_TRAINCODE="//a[@id='TrainCodeHyperLink']",
        XPATH_COM = "//span[@id='Comment']", XPATH_FAR = "//span[@id='Label1']";

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.schedule);
        Bundle reciBundle = this.getIntent().getExtras();
        String siteUrl = reciBundle.getString("url");
        new loadHtml().execute(siteUrl);
        //loadHtml.cancel(true);
    }

    private class loadHtml extends AsyncTask<String, Integer, ArrayList> {
        protected ArrayList doInBackground(String... urls) {
            try {
                HtmlCleaner cleaner = new HtmlCleaner();
                TagNode rootNode = cleaner.clean(new URL(urls[0]));
                TagNode[] trTag = rootNode.getElementsByAttValue("class", "Grid_Row", true, true);
                ArrayList<HashMap<String, Object>> mapList = new ArrayList<HashMap<String, Object>>();

                for(int i=1; i<trTag.length; i++)
                {
                    TagNode[] temp = trTag[i].getElementsByName("td", true);
                    HashMap<String, Object> map = new HashMap<String, Object>();
                    for(int j=0; j<temp.length-2; j++)
                    {
                        switch (j)
                        {
                            case 0:
                                map.put("train", ((TagNode) temp[j]
                                        .evaluateXPath(XPATH_TRAIN)[0]).getText().toString());
                                break;
                            case 1:
                                map.put("trainCode", ((TagNode) temp[j]
                                        .evaluateXPath(XPATH_TRAINCODE)[0]).getText().toString());
                                break;
                            case 2:
                                map.put("via", ((TagNode) temp[j]).getText().toString());
                                break;
                            case 3:
                                map.put("startEnd", ((TagNode) temp[j]).getText().toString());
                                break;
                            case 4:
                                map.put("startTime", ((TagNode) temp[j]).getText().toString());
                                break;
                            case 5:
                                map.put("endTime", ((TagNode) temp[j]).getText().toString());
                                break;
                            case 6:
                                map.put("allTime", ((TagNode) temp[j]).getText().toString());
                                break;
                            case 7:
                                TagNode[] icons= temp[j].getElementsByAttValue("class","smallicon", true, true);
                                Boolean[] strIcons={false,false,false,false};
                                for(int k=0; k<icons.length; k++)
                                {
                                    if(icons[k].getAttributeByName("src").toString().equals("images/handicapped.gif"))
                                        strIcons[0] = true;
                                    if(icons[k].getAttributeByName("src").toString().equals("images/bike.gif"))
                                        strIcons[1] = true;
                                    if(icons[k].getAttributeByName("src").toString().equals("images/breastfeeding.jpg"))
                                        strIcons[2] = true;
                                    if(icons[k].getAttributeByName("src").toString().equals("images/acrossnight.gif"))
                                        strIcons[3] = true;
                                }
                                map.put("handicappedIcon",strIcons[0]);
                                map.put("bikeIcon",strIcons[1]);
                                map.put("breastfeedingIcon",strIcons[2]);
                                map.put("acrossnightIcon",strIcons[3]);
                                map.put("comment", ((TagNode) temp[j]
                                        .evaluateXPath(XPATH_COM)[0]).getText().toString());

                                break;
                            case 8:
                                map.put("far", ((TagNode) temp[j]
                                        .evaluateXPath(XPATH_FAR)[0]).getText().toString());
                        }

                    }
                    Log.d("dddddddddd",map.toString());
                    mapList.add(map);
                }
                return mapList;

            } catch (MalformedURLException e) {
                e.printStackTrace();
                return null;
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            } catch (XPatherException e) {
                e.printStackTrace();
                return null;
            }

        }
        protected void onPostExecute(ArrayList result) {
            //show the UI
            mData = result;
            MyAdapter adapter=new MyAdapter(ScheduleActivity.this);
            ListView list_schedule = (ListView) findViewById(R.id.list_schedule);
            list_schedule.setAdapter(adapter);
        }
    }

    private class MyAdapter extends BaseAdapter{
        private LayoutInflater myInflater;
        public MyAdapter(Context ctxt){
            myInflater = LayoutInflater.from(ctxt);
        }
        @Override
        public int getCount() {
            return mData.size();
        }

        @Override
        public Object getItem(int i) {
            return mData.get(i);
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup viewGroup) {
            HashMap<String,Object> data = (HashMap<String,Object>)(this.getItem(position));
            convertView = myInflater.inflate(R.layout.myadapter,null);
            TextView text_train = (TextView) convertView.findViewById(R.id.text_train_type);
            String train = data.get("train").toString();

            text_train.setText(train);
            TextView text_code = (TextView) convertView.findViewById(R.id.text_train_code);
            String strTrainCode = data.get("trainCode").toString();
            if(train.equals("區間車")) {
                text_train.setTextColor(Color.parseColor("#1874CD"));
                text_code.setTextColor(Color.parseColor("#1874CD"));
            }
            if(train.equals("莒光")){
                text_train.setTextColor(Color.parseColor("#FF7F00"));
                text_code.setTextColor(Color.parseColor("#FF7F00"));
            }

            if(train.equals("自強")){
                text_train.setTextColor(Color.parseColor("#FF4040"));
                text_code.setTextColor(Color.parseColor("#FF4040"));
            }

            text_code.setText(strTrainCode);
            TextView text_via = (TextView) convertView.findViewById(R.id.text_via);
            String strTrainVia = data.get("via").toString();
            text_via.setText(strTrainVia);

            ImageView icon1 = (ImageView) convertView.findViewById(R.id.imageView1);
            ImageView icon2 = (ImageView) convertView.findViewById(R.id.imageView2);
            ImageView icon3 = (ImageView) convertView.findViewById(R.id.imageView3);
            ImageView icon4 = (ImageView) convertView.findViewById(R.id.imageView4);
//Log.d("aaaaaaaaaaaaaaaa",data.toString());
            if(data.get("handicappedIcon").toString().equals(Boolean.toString(true))) {
                icon1.setImageResource(R.drawable.handicapped);
                icon1.setPadding(0,0,10,0);
            }
            if(data.get("bikeIcon").equals(true)) {
                icon2.setImageResource(R.drawable.bike);
                icon2.setPadding(0,0,10,0);
            }
            if(data.get("breastfeedingIcon").equals(true)) {
                icon3.setImageResource(R.drawable.breastfeeding);
                icon3.setPadding(0,0,10,0);
            }

            if(data.get("acrossnightIcon").equals(true)) {
                icon4.setImageResource(R.drawable.acrossnight);
                icon4.setPadding(0,0,10,0);
            }


            TextView text_start = (TextView) convertView.findViewById(R.id.text_start);
            String strTrainStart = data.get("startTime").toString();
            text_start.setText(strTrainStart);
            TextView text_stop = (TextView) convertView.findViewById(R.id.text_stop);
            String strTrainStop = data.get("endTime").toString();
            text_stop.setText(strTrainStop);
            TextView text_time = (TextView) convertView.findViewById(R.id.text_time);
            String strTrainTime = data.get("allTime").toString();
            text_time.setText(strTrainTime);
            TextView text_far = (TextView) convertView.findViewById(R.id.text_far);
            String strTrainFar = data.get("far").toString();
            text_far.setText(strTrainFar);

            TextView text_comment = (TextView) convertView.findViewById(R.id.text_comment);
            String strTrainComm = data.get("comment").toString();
            text_comment.setText(strTrainComm);

            return convertView;
        }
    }

}

