package com.example.hw4;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.*;

import java.util.*;

public class MyActivity extends Activity {
    private Spinner spinnerStartCounty, spinnerStartStation, spinnerStopCounty, spinnerStopStation;
    private ArrayAdapter<CharSequence> adapterStartCounty, adapterStartStation, adapterStopCounty, adapterStopStation;
    private RadioGroup groupTrainClass;
    private EditText textDate, textFromTime, textToTime;
    private Button btnSearch;
    ArrayList<String> startStationCode, stopStationCode;
    private int startSelect, stopSelect;

    private String strTrainClass, strStartPos, strStopPos, strFromTime, strToTime,  strAllUrl;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        spinnerStartCounty = (Spinner) findViewById(R.id.start_county);
        adapterStartCounty = ArrayAdapter.createFromResource(this,R.array.area_name, android.R.layout.simple_list_item_1);
        adapterStartCounty.setDropDownViewResource(android.R.layout.simple_list_item_1);
        spinnerStartCounty.setAdapter(adapterStartCounty);
        spinnerStartCounty.setOnItemSelectedListener(startCountyListener);

        spinnerStopCounty = (Spinner) findViewById(R.id.stop_county);
        adapterStopCounty = ArrayAdapter.createFromResource(this,R.array.area_name, android.R.layout.simple_list_item_1);
        adapterStopCounty.setDropDownViewResource(android.R.layout.simple_list_item_1);
        spinnerStopCounty.setAdapter(adapterStopCounty);
        spinnerStopCounty.setOnItemSelectedListener(stopCountyListener);

        groupTrainClass = (RadioGroup) findViewById(R.id.train_group);
        groupTrainClass.setOnCheckedChangeListener(radioGroupListener);

        textDate = (EditText) findViewById(R.id.dateEdit);
        textDate.setOnClickListener(dateListener);
        textDate.setOnFocusChangeListener(dateFocusListener);
        textFromTime = (EditText) findViewById(R.id.timeFromEdit);
        textFromTime.setOnClickListener(fromTimeListener);
        textFromTime.setOnFocusChangeListener(fromTimeFocusListener);
        textToTime = (EditText) findViewById(R.id.timeToEdit);
        textToTime.setOnClickListener(toTimeListener);
        textToTime.setOnFocusChangeListener(toTimeFocusListener);
        btnSearch = (Button) findViewById(R.id.btnSearch);
        btnSearch.setOnClickListener(searchTrain);
    }
    private Button.OnClickListener searchTrain = new Button.OnClickListener() {
        @Override
        public void onClick(View view) {
            //http://twtraffic.tra.gov.tw/twrail/Search
            if(textDate.getText()!=null && strStartPos!=null && strStartPos!=null && strStopPos!=null && strTrainClass!=null && strFromTime!=null && strToTime!=null) {
                strAllUrl = "http://twtraffic.tra.gov.tw/twrail/SearchResult.aspx?searchtype=0&searchdate=" + textDate.getText() + strStartPos + strStopPos + strTrainClass + strFromTime + strToTime;
                //Log.d("dddddddddddd", strAllUrl);
                Intent intent = new Intent();
                intent.setClass(MyActivity.this, ScheduleActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("url", strAllUrl);
                intent.putExtras(bundle);
                startActivity(intent);
            }

        }
    };

    private  EditText.OnClickListener toTimeListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            changeToTime();
        }
    };
    private EditText.OnFocusChangeListener toTimeFocusListener = new View.OnFocusChangeListener() {
        @Override
        public void onFocusChange(View view, boolean b) {
            if(b)
                changeToTime();
        }
    };
    private  EditText.OnClickListener fromTimeListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            changeFromTime();
        }
    };
    private EditText.OnFocusChangeListener fromTimeFocusListener = new View.OnFocusChangeListener() {
        @Override
        public void onFocusChange(View view, boolean b) {
            if(b)
                changeFromTime();
        }
    };
    private EditText.OnClickListener dateListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            changeDate();

        }
    };
    private EditText.OnFocusChangeListener dateFocusListener = new View.OnFocusChangeListener() {
        @Override
        public void onFocusChange(View view, boolean b) {
            if(b)
               changeDate();
        }
    };

private RadioGroup.OnCheckedChangeListener radioGroupListener = new RadioGroup.OnCheckedChangeListener() {
    @Override
    public void onCheckedChanged(RadioGroup radioGroup, int checkedId) {
        switch (checkedId)
        {
            case R.id.all_train:
                strTrainClass = "&trainclass=2";
                break;
            case R.id.number_train:
                strTrainClass = "&trainclass='1100','1101','1102','1107','1110','1120'";
                break;
            case R.id.non_number_train:
                strTrainClass = "&trainclass='1131','1132','1140'";
                break;
        }

    }
};
    private Spinner.OnItemSelectedListener startCountyListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
            //position = spinnerStartCounty.getSelectedItemPosition();
            //Log.d("dddddddddddddddd", Integer.toString(position));
            String[] myResArray = null;
            List<String> myResArrayList;
            spinnerStartStation = (Spinner) findViewById(R.id.start_station);

            switch (position)
            {
                case 0:
                    myResArray = getResources().getStringArray(R.array.taipei_area_code);
                    adapterStartStation = ArrayAdapter.createFromResource(MyActivity.this,R.array.taipei_area, android.R.layout.simple_list_item_1);
                    startSelect = 18;
                    break;
                case 1:
                    myResArray = getResources().getStringArray(R.array.taoyuan_area_code);
                    adapterStartStation = ArrayAdapter.createFromResource(MyActivity.this,R.array.taoyuan_area, android.R.layout.simple_list_item_1);
                    startSelect = 0;
                    break;
                case 2:
                    myResArray = getResources().getStringArray(R.array.hsinchu_area_code);
                    adapterStartStation = ArrayAdapter.createFromResource(MyActivity.this,R.array.hsinchu_area, android.R.layout.simple_list_item_1);
                    startSelect = 4;
                    break;
                case 3:
                    myResArray = getResources().getStringArray(R.array.miaoli_area_code);
                    adapterStartStation = ArrayAdapter.createFromResource(MyActivity.this,R.array.miaoli_area, android.R.layout.simple_list_item_1);
                    startSelect = 12;
                    break;
                case 4:
                    myResArray = getResources().getStringArray(R.array.taichung_area_code);
                    adapterStartStation = ArrayAdapter.createFromResource(MyActivity.this,R.array.taichung_area, android.R.layout.simple_list_item_1);
                    startSelect = 13;
                    break;
                case 5:
                    myResArray = getResources().getStringArray(R.array.changhua_area_code);
                    adapterStartStation = ArrayAdapter.createFromResource(MyActivity.this,R.array.changhua_area, android.R.layout.simple_list_item_1);
                    startSelect = 0;
                    break;
                case 6:
                    myResArray = getResources().getStringArray(R.array.nantou_area_code);
                    adapterStartStation = ArrayAdapter.createFromResource(MyActivity.this,R.array.nantou_area, android.R.layout.simple_list_item_1);
                    startSelect = 3;
                    break;
                case 7:
                    myResArray = getResources().getStringArray(R.array.yunlin_area_code);
                    adapterStartStation = ArrayAdapter.createFromResource(MyActivity.this,R.array.yunlin_area, android.R.layout.simple_list_item_1);
                    startSelect = 2;
                    break;
                case 8:
                    myResArray = getResources().getStringArray(R.array.chiayi_area_code);
                    adapterStartStation = ArrayAdapter.createFromResource(MyActivity.this,R.array.chiayi_area, android.R.layout.simple_list_item_1);
                    startSelect = 3;
                    break;
                case 9:
                    myResArray = getResources().getStringArray(R.array.tainan_area_code);
                    adapterStartStation = ArrayAdapter.createFromResource(MyActivity.this,R.array.tainan_area, android.R.layout.simple_list_item_1);
                    startSelect = 11;
                    break;
                case 10:
                    myResArray = getResources().getStringArray(R.array.kaohsiung_area_code);
                    adapterStartStation = ArrayAdapter.createFromResource(MyActivity.this,R.array.kaohsiung_area, android.R.layout.simple_list_item_1);
                    startSelect = 7;
                    break;
                case 11:
                    myResArray = getResources().getStringArray(R.array.pingtung_area_code);
                    adapterStartStation = ArrayAdapter.createFromResource(MyActivity.this,R.array.pingtung_area, android.R.layout.simple_list_item_1);
                    startSelect = 1;
                    break;
                case 12:
                    myResArray = getResources().getStringArray(R.array.taitung_area_code);
                    adapterStartStation = ArrayAdapter.createFromResource(MyActivity.this,R.array.taitung_area, android.R.layout.simple_list_item_1);
                    startSelect = 7;
                    break;
                case 13:
                    myResArray = getResources().getStringArray(R.array.hualien_area_code);
                    adapterStartStation = ArrayAdapter.createFromResource(MyActivity.this,R.array.hualien_area, android.R.layout.simple_list_item_1);
                    startSelect = 17;
                    break;
                case 14:
                    myResArray = getResources().getStringArray(R.array.ilan_area_code);
                    adapterStartStation = ArrayAdapter.createFromResource(MyActivity.this,R.array.ilan_area, android.R.layout.simple_list_item_1);
                    startSelect = 12;
                    break;
                case 15:
                    myResArray = getResources().getStringArray(R.array.pinghsi_shenao_area_code);
                    adapterStartStation = ArrayAdapter.createFromResource(MyActivity.this,R.array.pinghsi_shenao_area, android.R.layout.simple_list_item_1);
                    startSelect = 1;
                    break;
                case 16:
                    myResArray = getResources().getStringArray(R.array.neiwan_liujia_area_code);
                    adapterStartStation = ArrayAdapter.createFromResource(MyActivity.this,R.array.neiwan_liujia_area, android.R.layout.simple_list_item_1);
                    startSelect = 13;
                    break;
                case 17:
                    myResArray = getResources().getStringArray(R.array.jiji_area_code);
                    adapterStartStation = ArrayAdapter.createFromResource(MyActivity.this,R.array.jiji_area, android.R.layout.simple_list_item_1);
                    startSelect = 4;
                    break;
                case 18:
                    myResArray = getResources().getStringArray(R.array.shalun_area_code);
                    adapterStartStation = ArrayAdapter.createFromResource(MyActivity.this,R.array.shalun_area, android.R.layout.simple_list_item_1);
                    startSelect = 1;
                    break;

            }
            myResArrayList = Arrays.asList(myResArray);
            startStationCode = new ArrayList<String>(myResArrayList);
            adapterStartStation.setDropDownViewResource(android.R.layout.simple_list_item_1);
            spinnerStartStation.setAdapter(adapterStartStation);
            spinnerStartStation.setOnItemSelectedListener(startStationListener);
            spinnerStartStation.setSelection(startSelect);
        }

        @Override
        public void onNothingSelected(AdapterView<?> adapterView) {

        }
    };

    private  Spinner.OnItemSelectedListener startStationListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
            spinnerStartStation = (Spinner) findViewById(R.id.start_station);
            //position = spinnerStartStation.getSelectedItemPosition();

           // Log.d("dddddddddddddddd", startStationCode.get(position).toString());
            strStartPos = "&fromstation=" + startStationCode.get(position).toString();
        }

        @Override
        public void onNothingSelected(AdapterView<?> adapterView) {

        }
    };
    private Spinner.OnItemSelectedListener stopCountyListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
            //position = spinnerStopCounty.getSelectedItemPosition();
            //Log.d("dddddddddddddddd", Integer.toString(position));
            String[] myResArray = null;
            List<String> myResArrayList;
            spinnerStopStation = (Spinner) findViewById(R.id.stop_station);

            switch (position)
            {
                case 0:
                    myResArray = getResources().getStringArray(R.array.taipei_area_code);
                    adapterStopStation = ArrayAdapter.createFromResource(MyActivity.this, R.array.taipei_area, android.R.layout.simple_list_item_1);
                    stopSelect = 18;
                    break;
                case 1:
                    myResArray = getResources().getStringArray(R.array.taoyuan_area_code);
                    adapterStopStation = ArrayAdapter.createFromResource(MyActivity.this,R.array.taoyuan_area, android.R.layout.simple_list_item_1);
                    stopSelect = 0;
                    break;
                case 2:
                    myResArray = getResources().getStringArray(R.array.hsinchu_area_code);
                    adapterStopStation = ArrayAdapter.createFromResource(MyActivity.this,R.array.hsinchu_area, android.R.layout.simple_list_item_1);
                    stopSelect = 4;
                    break;
                case 3:
                    myResArray = getResources().getStringArray(R.array.miaoli_area_code);
                    adapterStopStation = ArrayAdapter.createFromResource(MyActivity.this,R.array.miaoli_area, android.R.layout.simple_list_item_1);
                    stopSelect = 12;
                    break;
                case 4:
                    myResArray = getResources().getStringArray(R.array.taichung_area_code);
                    adapterStopStation = ArrayAdapter.createFromResource(MyActivity.this,R.array.taichung_area, android.R.layout.simple_list_item_1);
                    stopSelect = 13;
                    break;
                case 5:
                    myResArray = getResources().getStringArray(R.array.changhua_area_code);
                    adapterStopStation = ArrayAdapter.createFromResource(MyActivity.this,R.array.changhua_area, android.R.layout.simple_list_item_1);
                    stopSelect = 0;
                    break;
                case 6:
                    myResArray = getResources().getStringArray(R.array.nantou_area_code);
                    adapterStopStation = ArrayAdapter.createFromResource(MyActivity.this,R.array.nantou_area, android.R.layout.simple_list_item_1);
                    stopSelect = 3;
                    break;
                case 7:
                    myResArray = getResources().getStringArray(R.array.yunlin_area_code);
                    adapterStopStation = ArrayAdapter.createFromResource(MyActivity.this,R.array.yunlin_area, android.R.layout.simple_list_item_1);
                    stopSelect = 2;
                    break;
                case 8:
                    myResArray = getResources().getStringArray(R.array.chiayi_area_code);
                    adapterStopStation = ArrayAdapter.createFromResource(MyActivity.this,R.array.chiayi_area, android.R.layout.simple_list_item_1);
                    stopSelect = 3;
                    break;
                case 9:
                    myResArray = getResources().getStringArray(R.array.tainan_area_code);
                    adapterStopStation = ArrayAdapter.createFromResource(MyActivity.this,R.array.tainan_area, android.R.layout.simple_list_item_1);
                    stopSelect = 11;
                    break;
                case 10:
                    myResArray = getResources().getStringArray(R.array.kaohsiung_area_code);
                    adapterStopStation = ArrayAdapter.createFromResource(MyActivity.this,R.array.kaohsiung_area, android.R.layout.simple_list_item_1);
                    stopSelect = 7;
                    break;
                case 11:
                    myResArray = getResources().getStringArray(R.array.pingtung_area_code);
                    adapterStopStation = ArrayAdapter.createFromResource(MyActivity.this,R.array.pingtung_area, android.R.layout.simple_list_item_1);
                    stopSelect = 1;
                    break;
                case 12:
                    myResArray = getResources().getStringArray(R.array.taitung_area_code);
                    adapterStopStation = ArrayAdapter.createFromResource(MyActivity.this,R.array.taitung_area, android.R.layout.simple_list_item_1);
                    stopSelect = 7;
                    break;
                case 13:
                    myResArray = getResources().getStringArray(R.array.hualien_area_code);
                    adapterStopStation = ArrayAdapter.createFromResource(MyActivity.this,R.array.hualien_area, android.R.layout.simple_list_item_1);
                    stopSelect = 17;
                    break;
                case 14:
                    myResArray = getResources().getStringArray(R.array.ilan_area_code);
                    adapterStopStation = ArrayAdapter.createFromResource(MyActivity.this,R.array.ilan_area, android.R.layout.simple_list_item_1);
                    stopSelect = 12;
                    break;
                case 15:
                    myResArray = getResources().getStringArray(R.array.pinghsi_shenao_area_code);
                    adapterStopStation = ArrayAdapter.createFromResource(MyActivity.this,R.array.pinghsi_shenao_area, android.R.layout.simple_list_item_1);
                    stopSelect = 1;
                    break;
                case 16:
                    myResArray = getResources().getStringArray(R.array.neiwan_liujia_area_code);
                    adapterStopStation = ArrayAdapter.createFromResource(MyActivity.this,R.array.neiwan_liujia_area, android.R.layout.simple_list_item_1);
                    stopSelect = 13;
                    break;
                case 17:
                    myResArray = getResources().getStringArray(R.array.jiji_area_code);
                    adapterStopStation = ArrayAdapter.createFromResource(MyActivity.this,R.array.jiji_area, android.R.layout.simple_list_item_1);
                    stopSelect = 4;
                    break;
                case 18:
                    myResArray = getResources().getStringArray(R.array.shalun_area_code);
                    adapterStopStation = ArrayAdapter.createFromResource(MyActivity.this,R.array.shalun_area, android.R.layout.simple_list_item_1);
                    stopSelect = 1;
                    break;

            }
            myResArrayList = Arrays.asList(myResArray);
            stopStationCode = new ArrayList<String>(myResArrayList);
            adapterStopStation.setDropDownViewResource(android.R.layout.simple_list_item_1);
            spinnerStopStation.setAdapter(adapterStopStation);
            spinnerStopStation.setOnItemSelectedListener(stopStationListener);
            spinnerStopStation.setSelection(stopSelect);
        }

        @Override
        public void onNothingSelected(AdapterView<?> adapterView) {

        }
    };
    private Spinner.OnItemSelectedListener stopStationListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
            spinnerStopStation = (Spinner) findViewById(R.id.stop_station);
            //position = spinnerStopStation.getSelectedItemPosition();
            //Log.d("dddddddddddddddd", stopStationCode.get(position).toString());
            strStopPos = "&tostation=" + stopStationCode.get(position).toString();
        }

        @Override
        public void onNothingSelected(AdapterView<?> adapterView) {

        }
    };
    private void changeToTime() {
        final Calendar c = Calendar.getInstance();
        int mHour, mMinute;
        mHour = c.get(Calendar.HOUR_OF_DAY);
        mMinute = c.get(Calendar.MINUTE);
        // Launch Time Picker Dialog
        TimePickerDialog timePick = new TimePickerDialog(MyActivity.this,
                new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        //"%02", 0 means  fill 0, 2 means a least 2 digit..... if %2d" means fill space
                        textToTime.setText(String.format("%02d",hourOfDay) + ":" + String.format("%02d",minute));
                        strToTime = "&totime="+ String.format("%02d",hourOfDay) + String.format("%02d",minute);
                    }
                }, mHour, mMinute, false);
        timePick.show();
    }
    private void changeFromTime() {
        final Calendar c = Calendar.getInstance();
        int mHour, mMinute;
        mHour = c.get(Calendar.HOUR_OF_DAY);
        mMinute = c.get(Calendar.MINUTE);
        // Launch Time Picker Dialog
        TimePickerDialog timePick = new TimePickerDialog(MyActivity.this,
                new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        textFromTime.setText(String.format("%02d",hourOfDay) + ":" + String.format("%02d",minute));
                        strFromTime = "&fromtime=" + String.format("%02d",hourOfDay) + String.format("%02d",minute);
                    }
                }, mHour, mMinute, false);
        timePick.show();
    }
    private void changeDate(){
        final int mYear, mMonth, mDay;
        final Calendar c = Calendar.getInstance();
        c.setTime(new Date());
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePicker = new DatePickerDialog(MyActivity.this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                textDate.setText(year + "/" + String.format("%02d",(monthOfYear + 1)) + "/" + String.format("%02d",dayOfMonth));
            }
        }, mYear, mMonth, mDay
        );
        //set min day
        datePicker.getDatePicker().setMinDate(c.getTimeInMillis());
        //set max day
        c.add(Calendar.DAY_OF_MONTH, 45);
        datePicker.getDatePicker().setMaxDate(c.getTimeInMillis());
        datePicker.show();
    }
}
