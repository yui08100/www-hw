<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=BIG5">
<title>HW6</title>
<link
	href="http://netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css"
	rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
	<script type="text/javascript" src="searchTrip.js"></script>
</head>
<body>
	<div class="form col-lg-4 col-lg-offset-4">
		<h1>Trip Search</h1>
		<div class="input-group" id="input-group-id">
			<input type="text" class="form-control" id="textSearch" /> <span
				class="input-group-btn"> <input type="button" value="Search"
				id="btnSearch" class="btn btn-primary">
			</span>
		</div>
	</div>
	<table id="resultTable" class="table table-striped">
	</table>
</body>
</html>