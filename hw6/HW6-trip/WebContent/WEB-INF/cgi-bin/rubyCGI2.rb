#!/usr/bin/env ruby
print "Content-type: text/xml\n\n"

require 'cgi'
cgi=CGI.new
cgi.keys.each do |p|
	$target = cgi[p]
end

if $target =~ /(.*)\s(.*)/
	splitTarget=$target.split(' ')
	$target = splitTarget[0]
	for $i in 1..splitTarget.length-1
		$target += '%20' + splitTarget[$i]
	end
end


$theName = ''
$location = ''
$picture = ''
$rating = 0
$viewed = ''
$url = ''

$Arr_theName = []
$Arr_location = []
$Arr_picture = []
$Arr_rating = []
$Arr_viewd = []
$Arr_url = []

$foundIt = false

$getTheNextLine = false

$getTheNextLineForView = false
def getViewed()
	url = "http://www.everytrail.com" + $url
	open(url) {|f|
	f.each_line {|line|
		if $getTheNextLineForView
			$getTheNextLineForView = false
			if line =~ /(.*)Viewed(.)(.*)(.)times(.*)/
			$viewed = $3
			end
		end	
		if line =~ /(.*)class(.*)style="margin-right:10px"(.*)/
			$getTheNextLineForView = true
		end
	}
	}
end

print '<?xml version="1.0" encoding="UTF-8"?>'
print '<trips>'

require 'open-uri'
open("http://www.everytrail.com/search.php?q=" + $target) {|f|	
	f.each_line {|line|
		begin
		if line =~ /(.*)div class(.*)meta(.*)/	
			
			$foundIt = true

			#Print out session
			$url = "http://www.everytrail.com" + $url
			
			$theName = $theName.strip
			$location = $location.strip
			$picture = $picture.strip
			$url = $url.strip

			if $theName.include? "&"
				$theName["&"] = "&amp;"
			end
			if $location.include? "&"
				$location["&"] = "&amp;"
			end
			if $picture.include? "&"
				$picture["&"] = "&amp;"
			end
			if $url.include? "&"
				$url["&"] = "&amp;"
			end
			print '<trip>'
			print '<name>' + $theName + '</name>'
			print '<location>' + $location + '</location>'
			print '<rating>' + $rating.to_s + '</rating>'
			print '<viewed>' + $viewed + '</viewed>'
			print '<picture>' + $picture + '</picture>'
			print '<url>' + $url + '</url>'
			print '</trip>'
			#Print out session end==
			

			$theName = ""
			$rating = 0
			$picture = ""
			$viewd = ""
			$url = ""
			$location = ""
		end

		if line =~ /(.*)img src="(.*)" width(.*)alt(.*)title(.*)/
			$picture = $2
		end		
		
		if line =~ /(.*)a href='(.)guide(.)(.*)'>(.*)<(.)a>/
			$theName = $5
			
			$url = "/guide/" + $4
			getViewed()
		end

		if $getTheNextLine
			$getTheNextLine = false
			$location = line
		end
		if line =~ /(.*)div class(.*)small-text(.)light-text(.*)/
			$getTheNextLine = true
		end

		if line =~ /(.*)img class(.*)star-full cursor(.*)title='(.)(.)5'(.*)/
			$rating = $4
		end
		
		rescue
		end
	}
}

print '</trips>'
