package web.servlet.trip;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.URL;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSON;
import net.sf.json.xml.XMLSerializer;



/**
 * Servlet implementation class TripSearch
 */
public class TripSearch extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public TripSearch() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		response.setCharacterEncoding("UTF-8");
        PrintWriter out = response.getWriter();
        /*get the input string*/
        String keyword = request.getParameter("keyword");
        String target_url = "http://dmplus.cs.ccu.edu.tw:49328/cgi-bin/rubyCGI2.rb/trip?scenery=" + keyword;
        
        /*get the url's context(XML)*/
        URL oracle = new URL(target_url);
        BufferedReader in = new BufferedReader(
        new InputStreamReader(oracle.openStream()));
        
        String target_xml ="",inputLine;
        while ((inputLine = in.readLine()) != null) {
        	target_xml += inputLine;
        }
        in.close();
        
        /*let XMLSerializer parse the XML to json, it will output JSONArray*/
        XMLSerializer xmlSerializer = new XMLSerializer(); 
        JSON json = xmlSerializer.read(target_xml); 
        out.print(json.toString());
		out.close();
	}

}
