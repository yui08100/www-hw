<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template match="/">
  <html>
  <body >
  <div align="center">
<xsl:for-each select="mixedteams/football">
    <table border="1" width="550px" style="text-align:center;" >
      <tr>
        <th colspan="4">
		<font color="{Color}">
			<h2><xsl:value-of select="Team"/></h2>
		</font>
		</th>
      </tr>
      <tr  bgcolor="{Color}" style="font-weight:bold">
        <td width="100px">Image</td>
        <td width="250px">Striker</td>
		<td width="100px">Manager</td>
		<td width="100px">Stadium</td>
      </tr>
	  <tr bgcolor="{Color}">
        <td ><img src="{Image}" height="65" width="55" style="padding:10px"/></td>
        <td style="display:block;padding:0px">
		<table border="1" width="250px" height="100px" style="text-align:center;">
			<tr >
				<td style="word-wrap:break-word" width="100px"><xsl:value-of select="Striker/name"/></td>
				<td><xsl:value-of select="Striker/age"/></td>
				<td><xsl:value-of select="Striker/squadnum"/></td>
			</tr>
		</table>
		</td>
		<td ><xsl:value-of select="Manager"/></td>
        <td ><xsl:value-of select="Stadium"/></td>
      </tr>
	  <tr>
		  <td colspan="4">
		  <iframe width="550px" height="360px" src="{video}" frameborder="0" ></iframe>
		  </td>
	  </tr>
    </table>
	</xsl:for-each>
	</div>
  </body>
  </html>
</xsl:template>
</xsl:stylesheet>