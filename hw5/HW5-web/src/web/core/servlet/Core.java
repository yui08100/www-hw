package web.core.servlet;
import java.io.*; 
import java.net.*;
import java.util.*;
import java.net.URLConnection;

import org.htmlcleaner.CleanerProperties;
import org.htmlcleaner.HtmlCleaner;
import org.htmlcleaner.TagNode;
public class Core implements Externalizable
{
   	/*For saving each downloaded html content*/ 
	Hashtable<String, String> hHtmls = new Hashtable<String, String>();
	private Queue<String> qUrl;
	private int iCrawLink,iCrawVisit,iSearNum;
    /*The constructor*/
	public Core() {
		qUrl = new LinkedList<String>();
		hHtmls = new Hashtable<String, String>();
		iCrawLink=0;
		iCrawVisit=0;
		iSearNum=0;
	}
	
	/*The crawler funciton*/	
 	public String crawler(String url, int max) {
 		String urls = "", context = "", now_url="";
 		if(max==0)
 			return "no links";
 		/*the input page*/
		qUrl.add(url);
		context = HTTPConnection(url, max);
		hHtmls.put(url,context);
		urls += "<a href='" + url + "'>"+ url +"</a><br/>";
		iCrawLink++;
		/*visited HTML content on the queue(BFS), until hash table filled*/
		while(hHtmls.size() < max)
		{
			if(qUrl.isEmpty())
				break;
			now_url = qUrl.poll();
			iCrawVisit++;
	        try {
	        	context = HTTPConnection(now_url, max);
	        	HtmlCleaner cleaner = new HtmlCleaner();
				TagNode rootNode = cleaner.clean(context);
				//find all "a" tag element
				for(TagNode node : rootNode.getElementsByName("a", true))
				{
					String hrefStr = node.getAttributeByName("href");
					if((qUrl.size()==max)||(hHtmls.size()==max))
						break;
					// if the url isn't web pag, skip it
					if((hrefStr.lastIndexOf(".PNG")>(hrefStr.length()-5)) ||
						(hrefStr.lastIndexOf(".JPG")>(hrefStr.length()-5)) ||
						(hrefStr.lastIndexOf(".DOC")>(hrefStr.length()-5)) ||
						(hrefStr.lastIndexOf(".PDF")>(hrefStr.length()-5)) ||
						(hrefStr.lastIndexOf(".png")>(hrefStr.length()-5)) ||
						(hrefStr.lastIndexOf(".jpg")>(hrefStr.length()-5)) ||
						(hrefStr.lastIndexOf(".pdf")>(hrefStr.length()-5)) ||
						(hrefStr.lastIndexOf(".doc")>(hrefStr.length()-5)))
						continue;
					//crawl links
					if(hrefStr.contains("http")&&(!qUrl.contains(hrefStr))&&(!hHtmls.containsKey(hrefStr)))
					{
						
						URL real_url = new URL(hrefStr);
						if(real_url != null)
						{
							qUrl.add(hrefStr);
							urls += "<a href='" + hrefStr + "'>"+ hrefStr +"</a><br/>";
							context = HTTPConnection(now_url, max);
							hHtmls.put(hrefStr,context);
							iCrawLink++;
						}
					}
				}
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}			
		}
		/*Output the content of the crawled pages*/
		try {
			FileOutputStream fOut = new FileOutputStream("temp.txt");
			ObjectOutput out = new ObjectOutputStream(fOut);
			writeExternal(out);
		} catch(Exception e) { System.out.println(e); }
		//return a url list
		return urls;
	}
 	/*get the number of link that crawler really crawl*/
	public Integer getCrawlLinkNum() {
		return iCrawLink;
	}
	/*get the number of link that crawler really visited*/
	public Integer getCrawlVisitNum() {
		return iCrawVisit;
	}
	/*The search function*/	
	public String search(String keyword)
	{	
		String urls = "";
		try {
			/*Read the temp.txt where the system stores the crawler result*/
			FileInputStream fIn = new FileInputStream("temp.txt");
			ObjectInput in = new ObjectInputStream(fIn);
			readExternal(in);
			
			for(String search_url : hHtmls.keySet())
			{
				if(hHtmls.get(search_url).contains(keyword))
				{
					urls += search_url+"<br/>";
					iSearNum++;
				}
			}
			
			
		} catch(Exception e) { System.out.println(e); }
		//return a url list
		return urls;
	}
	/*get the number of link that searcher really catch keyword*/
	public Integer getSearchNum() {
		return iSearNum;
	}
	/*Read data from a file*/
    public void readExternal(ObjectInput in) throws IOException,
    	ClassNotFoundException
    {
    	hHtmls = (Hashtable) in.readObject();	
    }

	/*Write data into a file*/
    public void writeExternal(ObjectOutput out) throws IOException
    {
    	out.writeObject(hHtmls);
    	out.flush();
 		out.close();
	}

/**
 * Establish a http connection between Crawler and Internet,
 * send request to the URL and the return HTML page String.
 *
 * @param url - the HTML page url going to request
 * @return String - the HTML contents in String format
 *
 */	
    String HTTPConnection (String urlStr, int max) {
		
		// 1. check if protocol is http or not,
		//    currently we only search for http
    	// 2. establish a connection and check if its content type equals
    	//    to text/html
    	// 3. read html content, then return string back
    	try {

    		HtmlCleaner htmlcleaner = new HtmlCleaner();
    		TagNode root = htmlcleaner.clean(new URL(urlStr));
    		
    		HtmlCleaner cleaner = new HtmlCleaner();
			CleanerProperties properties = cleaner.getProperties();
			properties.setPruneTags("script,style");
			String all_context = htmlcleaner.getInnerHtml(root);
    		return refactory(all_context);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}		   
	}
    /*restruct the html code*/
	 String refactory(String htmlSource) {
	        HtmlCleaner cleaner = new HtmlCleaner();
	        // set clean the style and script tag
	        CleanerProperties p = cleaner.getProperties();
	        p.setPruneTags("style,script"); 
	 
	        TagNode node = null;
	        try {
	            node = cleaner.clean(htmlSource);
	        } catch (Exception e) {
	            e.printStackTrace();
	        }
	        return cleaner.getInnerHtml(node);
	    }	
}