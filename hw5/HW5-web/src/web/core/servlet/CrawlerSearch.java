package web.core.servlet;
import java.io.*;
import java.util.*;

import javax.jws.WebService;
import javax.servlet.*;
import javax.servlet.http.*;

@SuppressWarnings("serial")
public class CrawlerSearch extends HttpServlet {

	public void doGet(HttpServletRequest request, HttpServletResponse response)
    throws IOException, ServletException
    {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.println("Only support a Post Method.");
    }

	public void doPost(HttpServletRequest request, HttpServletResponse response)
    throws IOException, ServletException
    {
        PrintWriter out = response.getWriter();
        String action = request.getParameter("action");
        Core theCore = new Core();
        if(action.equals("crawler")) {
	        /*Get the two parameters from the crawler.htm page*/
	        String url = request.getParameter("url");
			String max = request.getParameter("max");
			if(!url.equals("") && !max.equals("")) {
				//call your Crawler function here 
				String urls = theCore.crawler(url, Integer.parseInt(max));
				Integer links = theCore.getCrawlLinkNum();
				Integer visits = theCore.getCrawlVisitNum();
				HttpSession session = request.getSession(true);
				session.putValue("urls",urls);
				session.putValue("links",links);
				session.putValue("visits",visits);
				/*Forward to the jsp to handlel the dynamic page layout*/
				getServletConfig().getServletContext().getRequestDispatcher("/crawler.jsp").forward(request, response);
			}
	    }
		else if(action.equals("search")) {
			/*Get the parameter from the search.htm page*/
			String keyword = request.getParameter("keyword");
			if(!keyword.equals("")) {
				//call your Search function here 
				String urls = theCore.search(keyword);	
				Integer slinks = theCore.getSearchNum();
				HttpSession session = request.getSession(true);
				session.putValue("urls_search",urls);
				session.putValue("slinks",slinks);
				/*Forward to the jsp to handlel the dynamic page layout*/
				getServletConfig().getServletContext().getRequestDispatcher("/search.jsp").forward(request, response);
			}
		}	
    }
}
