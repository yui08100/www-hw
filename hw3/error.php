<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>HW3</title>
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css" rel="stylesheet">
    <link href="style.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    
  </head>

  <body class="small_page">

    <div class="container well">
      <form method="post" action="signInAcc.php">
        <div class="form-signin">
        <h1>Error</h1>
        <p class="worrong_tag">
          Fail connection<br/>
          your account or password is invalid.
        </p> 
        <input type="submit" class="btn btn-danger" name="backToSign" value="Back to sign in" />
        </div>
      </form>
    </div>

  </body>
</html>