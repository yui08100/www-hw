<?php session_start(); ?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>HW3</title>
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css" rel="stylesheet">
    <link href="style.css" rel="stylesheet">
  </head>

  <body class="small_page">

    <div class="container well small_page">

      <form method="post" action="accEdit.php">
        <div class="form-signin">
          <h1>Profile</h1>
          <input name="accName" type="name" class="form-control" value="<?= $_SESSION['accountName']?>" readonly/>
          <input name="accPassWd" type="password" class="form-control" placeholder="Password"/>
          <input name="accEmail" type="email" class="form-control" value="<?= $_SESSION["accountEmail"]?>" />
          <input class="btn btn-info" type="submit" name="savePro" value="Update" />
          <input class="btn btn-warning" type="submit" name="cancelIt" value="Cancel" />
        </div>
      </form>
    </div>




  </body>
</html>

