WWW Technologies and Applications 2014
HOMEWORK#3
NAME : 黃喬苡
CCU ID: 602410086


(a)
Files : sign.php signInAcc.php create.php profile.php accEdit.php error.php
		post.php logOut.php command.php likeNumInc.php editPost.php postIt.php editInlinePost.php
		style.css
共 13個 .php檔 1個.css檔

會顯示在網頁的檔案共有: ()內為輔助用php,使用者看不到
sign.php - 登入頁面,可外連至create.php,登入後會存cookie,進入post.php
	(signInAcc.php)- sign.php輸入的帳號密碼驗證是否為正確的帳號密碼
create.php - 註冊頁面,可導回sign.php,註冊後會存cookie,進入post.php
profile.php - 個人資料編輯,自動填入帳號與email,且帳號不能改變
	(accEdit.php) - 會偵測重複的帳號,若註冊帳號需使用英文或數字,更新profile.php個人資料
error.php - 登入失敗畫面,可回sign.php
post.php - 留言版畫面,留言照著時間排順序,若沒登入會回到sign.php
	(logOut.php) - 登出,並回到sign.php
	(command.php) - 回覆別人留言,並同時會寄送一封信給原作者
	(likeNumInc.php) - 留言或回覆的like的數字+1
editPost.php - 編輯自己的post的畫面
	(postIt.php) - 當PO出一則新留言時,儲存此post,並可修改或刪除自己的post
	(editInlinePost.php) - 要編輯留言時將自己的留言從資料庫撈出來
style.css - 自己的css樣式設計	

(b)
account - 記錄帳號資料,name 與id 皆為 primary key
id name password email
post - 留言資料,id為 primary key, name為account的name
id name postStr likenum postTime
command - 回覆post, id為 primary key, name為account的name, postId為post的id
id name commandStr postId likenum postTime
	
(c)
Link :  http://dmplus.cs.ccu.edu.tw:49328/HW3/sign.php 
	
(d)
advanced features皆完成
1. 留言時間使用datetime的type完成
2. preg_match("/\w/", $accName)
3. mailto: post者的email
4. 刪除時記錄post的id將相關command的資料也都刪掉






