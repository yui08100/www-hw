<?php 
  $postId = $_GET['postId'];
  Require('editInlinePost.php');
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>HW3</title>
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css" rel="stylesheet">
    <link href="style.css" rel="stylesheet">
  </head>

  <body class="small_page">

    <div class="container well small_page">

      <form method="post" action="postIt.php?postId=<?= $postId ?>">
        <div class="form-signin">
          <h1>Edit Post</h1>

          <textarea class="form-control" rows="3" name="editPostFiled" required><?= $PostStr ?></textarea>
          <input class="btn btn-info" type="submit" name="savePost" value="Update" />
          <input class="btn btn-warning" type="submit" name="cancelIt" value="Cancel" />
        </div>
      </form>
    </div>




  </body>
</html>