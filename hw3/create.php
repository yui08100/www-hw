<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>HW3</title>
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css" rel="stylesheet">
    <link href="style.css" rel="stylesheet">
  </head>

  <body class="small_page">

    <div class="container well small_page">
      <form method="post" action="accEdit.php">
        <div class="form-signin">
          <h1>Create account</h1>
          or<a href="sign.php"> sign in</a>
          <input name="accName" type="name" class="form-control" placeholder="Name" required autofocus>
          <input name="accPassWd" type="password" class="form-control" placeholder="Password" required>
          <input name="accEmail" type="email" class="form-control" placeholder="Email address" required>
          <input class="btn btn-primary" type="submit" name="submit" value="Create account" />
        </div>
      </form>
    </div>
  </body>
</html>

